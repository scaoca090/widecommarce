package com.wide.ecommerce.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import com.wide.ecommerce.domain.Product;
import com.wide.ecommerce.repositories.ProductRepository;

public class ProductRepositoryMySql implements ProductRepository{

	private Connection conn;

	public ProductRepositoryMySql() {
		super();
	}
	public ProductRepositoryMySql(Connection conn) {
		this.conn = conn;
	}
	@Override
	public List<Product> findAll() {
		List<Product> product = new ArrayList<Product>();

		//driver registration 
		try {
			String sqlQuery = "SELECT * FROM product_table";
			PreparedStatement stm = conn.prepareStatement(sqlQuery);
			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String type = rs.getString("type");
				double price = rs.getDouble("price");
				String code = rs.getString("code");

				Product item = new Product(id, name, type, price, code);
				product.add(item);
			} 
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		return product;
	}

	@Override
	public Product findByCode(String code) {
		Product product = null;


		//driver registration 
		try {

			String sqlQuery = "SELECT * FROM product_table WHERE code = ? ";
			PreparedStatement stm = conn.prepareStatement(sqlQuery);
			stm.setString(1, code);
			ResultSet rs = stm.executeQuery();


			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String type = rs.getString("type");
				double price = rs.getDouble("price");
				String codepr = rs.getString("code");

				product = new Product(id, name, type, price, codepr);


			} 
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		return product;
	}

	@Override
	public int save(Product product) {

		int result = 0;
		//driver registration 
		try {
			String sql = "INSERT INTO  product_table (name, type, price, code) VALUES (?,?,?,?)";
			PreparedStatement prepStm = conn.prepareStatement(sql);
			prepStm.setString(1, product.getName());
			prepStm.setString(2, product.getType());
			prepStm.setDouble(3, product.getPrice());
			prepStm.setString(4, product.getCode());

			result = prepStm.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		return result;
	}
	@Override
	public int updateProduct(Product product) {
		int update = 0;
		try {
			String sql = "UPDATE product_table SET name = ?,type = ?,price=? WHERE code=? ";
			PreparedStatement prepared = conn.prepareStatement(sql);
			prepared.setString(1, product.getName());
			prepared.setString(2, product.getType());
			prepared.setDouble(3, product.getPrice());
			prepared.setString(4, product.getCode());

			update = prepared.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return update;
	}
	@Override
	public void deleteByCode(String code) {

		try {
			String sql = "DELETE FROM product_table WHERE code=?";
			PreparedStatement prepared = conn.prepareStatement(sql);
			prepared.setString(1, code);
			prepared.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();

		}

	}	
	
	@Override
	public Product findById(int id) {
		Product product = null;

		//driver registration 
		try {
			String sqlQuery = "SELECT * FROM product_table WHERE id = ? ";
			PreparedStatement stm = conn.prepareStatement(sqlQuery);
			stm.setInt(1, id);
			ResultSet rs = stm.executeQuery();
			while (rs.next()) {
				
				String name = rs.getString("name");
				String type = rs.getString("type");
				double price = rs.getDouble("price");
				String codepr = rs.getString("code");

				product = new Product(id, name, type, price, codepr);
			} 
		}	catch (SQLException e) {
				e.printStackTrace();
			}	
		return product;
		}
	}