package com.wide.ecommerce.repositories;

import java.util.List;

import com.wide.ecommerce.domain.Product;

public interface ProductRepository {
	public abstract List<Product> findAll();
	public abstract Product findByCode(String Code);
	public abstract Product findById(int id);
	public abstract int save (Product product);
	public abstract int updateProduct (Product product);
	public abstract void deleteByCode(String code);
	
}
