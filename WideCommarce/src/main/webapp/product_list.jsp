<%@page import="com.wide.ecommerce.domain.Product"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<link>
<head>

<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<%

%>
<a href="product.do?action=view">New Product</a>

<table border="10" width = "100%"> 
<thead>
<tr>
<th>No.</th>
<th>Code</th>
<th>Name</th>
<th>Type</th>
<th>Price</th>
<th>Action</th>
</tr>
</thead>
<c:forEach items="${product_data }" var="prod"> 
<tr>
		<td><c:out value="${prod.id}"/> </td>		
		<td><c:out value="${prod.code}"/> </td>
		<td><c:out value="${prod.name}"/> </td>
		<td><c:out value="${prod.type}"/> </td>
		<td><c:out value="${prod.price}"/> </td>
		<td><a href="/WideCommerce/product.do?action=edit&code=<c:out value="${prod.code}"/>"><button>Update</button></a> &nbsp; 
		<a href="/WideCommerce/product.do?action=delete&code=<c:out value="${prod.code}"/>"><button>Delete</button></a> </td>
</tr>		
</c:forEach>	

		
		

</table>



</body>
</html>
